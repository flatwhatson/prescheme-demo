/*
 * Call Pre-Scheme static initialization before main.
 */
__attribute__((constructor))
void ps_init(void);
