;;; ps-utils --- Utilities for Pre-Scheme

(define-syntax when
  (syntax-rules ()
    ((_ condition consequent ...)
     (if condition
         (begin consequent ...)))))

(define-syntax unless
  (syntax-rules ()
    ((_ condition antecedent ...)
     (if (not condition)
         (begin antecedent ...)))))

(define-syntax define-wrapper-type
  (lambda (exp rename compare)
    (define (symbol-append . args)
      (string->symbol
       (apply string-append (map (lambda (s)
                                   (if (string? s) s (symbol->string s)))
                                 args))))
    (let* ((name (cadr exp))
           (type-id (symbol-append ":" name))
           (constructor (rename (symbol-append "make-" name)))
           (%begin (rename 'begin))
           (%define-record-type (rename 'define-record-type))
           (%define-external (rename 'define-external)))
      `(,%define-record-type ,name ,type-id
        (,constructor)))))

(define (zero? n) (= n 0))
(define (one? n) (= n 1))
