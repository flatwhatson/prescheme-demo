;;; ps-sdl2 --- Pre-Scheme wrappers for SDL2

;;; Core

(define SDL_INIT_VIDEO (external "SDL_INIT_VIDEO" integer))

(define (sdl-init flags)
  (zero? ((external "SDL_Init" (=> (integer) integer)) flags)))

(define (sdl-quit)
  ((external "SDL_Quit" (=> () unit))))

(define (sdl-get-error)
  ((external "SDL_GetError" (=> () (^ char)))))

(define (sdl-delay ms)
  ((external "SDL_Delay" (=> (integer) unit)) ms))

;;; Window

(define-wrapper-type sdl-window)

(define SDL_WINDOWPOS_CENTERED (external "SDL_WINDOWPOS_CENTERED" integer))
(define SDL_WINDOW_SHOWN (external "SDL_WINDOW_SHOWN" integer))

(define (sdl-create-window title x y w h flags)
  ((external "PS_SDL_CreateWindow" (=> ((^ char) integer integer integer integer integer) sdl-window))
   title x y w h flags))

(define (sdl-destroy-window window)
  ((external "PS_SDL_DestroyWindow" (=> (sdl-window) unit))
   window))

;;; Renderer

(define-wrapper-type sdl-renderer)

(define SDL_RENDERER_ACCELERATED (external "SDL_RENDERER_ACCELERATED" integer))

(define (sdl-create-renderer window index flags)
  ((external "PS_SDL_CreateRenderer" (=> (sdl-window integer integer) sdl-renderer))
   window index flags))

(define (sdl-destroy-renderer renderer)
  ((external "PS_SDL_DestroyRenderer" (=> (sdl-renderer) unit))
   renderer))

(define (sdl-set-render-draw-color renderer r g b a)
  (zero? ((external "PS_SDL_SetRenderDrawColor" (=> (sdl-renderer integer integer integer integer) integer))
          renderer r g b a)))

(define (sdl-render-clear renderer)
  (zero? ((external "PS_SDL_RenderClear" (=> (sdl-renderer) integer))
          renderer)))

(define (sdl-render-present renderer)
  ((external "PS_SDL_RenderPresent" (=> (sdl-renderer) unit))
   renderer))

(define (sdl-render-fill-rect renderer x y w h)
  (zero? ((external "PS_SDL_RenderFillRect" (=> (sdl-renderer integer integer integer integer) integer))
          renderer x y w h)))

;;; Event

(define-wrapper-type sdl-event)

(define SDL_QUIT (external "SDL_QUIT" integer))

(define (sdl-create-event)
  ((external "PS_SDL_CreateEvent" (=> () sdl-event))))

(define (sdl-destroy-event event)
  ((external "PS_SDL_DestroyEvent" (=> (sdl-event) unit))
   event))

(define (sdl-poll-event event)
  (one? ((external "PS_SDL_PollEvent" (=> (sdl-event) integer))
         event)))

(define (sdl-event-type event)
  ((external "PS_SDL_EventType" (=> (sdl-event) integer))
   event))
