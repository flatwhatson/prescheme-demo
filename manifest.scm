(use-modules (guix packages)
             (guix profiles)
             (guix build-system gnu)
             (guix build utils)
             (guix gexp)
             (guix git)
             (guix utils)
             ((guix licenses) #:prefix license:)
             (gnu packages autotools)
             (gnu packages base)
             (gnu packages commencement)
             (gnu packages llvm)
             (gnu packages pkg-config)
             (gnu packages scheme)
             (gnu packages sdl)
             (gnu packages texinfo)
             (ice-9 popen)
             (ice-9 textual-ports))

(define-public scheme48-r7rs
  (package
   (name "scheme48-r7rs")
   (version "0.1")
   (source (git-checkout
            (url "https://codeberg.org/prescheme/s48-r7rs.git")
            (recursive? #t)))
   (build-system gnu-build-system)
   (inputs
    (list scheme48))
   (native-inputs
    (list autoconf-2.69 automake))
   (native-search-paths
    (list (search-path-specification
           (variable "SCHEME48_LOAD_PATH")
           (files '("share/scheme48-r7rs-0.1")))))
   (home-page "https://codeberg.org/prescheme/s48-r7rs")
   (synopsis "Incomplete R7RS implementation for Scheme 48")
   (description
    "This package provides an (incomplete) implementation of R7RS Small on
top of the venerable Scheme 48, providing:
 - support for loading R7RS libraries from the filesystem
 - support for running R7RS programs (aka. scripts)
 - implementations of the majority of R7RS-small core libraries
 - implementations of a handful of newer SRFIs (hash-tables, generators, etc.)")
   (license license:bsd-3)))

(define-public scheme48-prescheme
  (package
   (name "scheme48-prescheme")
   (version "0.1")
   (source (git-checkout
            (url "https://codeberg.org/prescheme/prescheme.git")))
   (build-system gnu-build-system)
   (inputs
    (list scheme48-r7rs))
   (native-inputs
    (list autoconf-2.69 automake libtool texinfo))
   (home-page "https://prescheme.org")
   (synopsis "Pre-Scheme compiler from Scheme 48, ported to R7RS")
   (description
    "Pre-Scheme is a statically compilable dialect of Scheme, originally used
to implement the Scheme 48 virtual machine.  This version of the compiler has been ported
to run on R7RS Scheme implementations, and runs on top of the Scheme 48 R7RS compatibility layer.")
   (license license:bsd-3)))

(packages->manifest
 (list scheme48-r7rs
       scheme48-prescheme
       gcc-toolchain
       gnu-make
       pkg-config
       clang ;; for clang-format
       sdl2
       ))
