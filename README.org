* Pre-Scheme Demo Code

This repository contains some basic examples of Pre-Scheme code, and a
simple Makefile for compiling these into native executables.

These examples are based on the one provided in "The Nearly Complete
Scheme48 PreScheme Reference Manual" by Taylor Campbell:
https://groups.scheme.org/prescheme/1.3/

** Source files

- packages.scm - package definitions
- lib/ps-utils.scm - utility macros
- lib/ps-string.scm - string utility functions
- lib/ps-vector.scm - vector functions based on SRFI-43
- lib/ps-grid.scm - an ASCII grid data-type
- lib/ps-sdl2.scm - minimal SDL2 bindings

- hello.scm - "Hello World" in Pre-Scheme, from the manual
- append.scm - Yes, you can string-append in Pre-Scheme
- vecfun.scm - Showing off Pre-Scheme polymorphism with vectors
- recfun.scm - Simple demonstration of records (structs)
- btree.scm - Example of a recursive record type
- game-of-life.scm - Conway's Game of Life in Pre-Scheme with SDL2
- prime-sum.scm - Sum of primes using Sieve of Eratosthenes

The generated C for each of the demo programs is also included, so you
can review the code generation without needing to build anything.

** How to build

The Guix manifest sets up all prerequisites for building the demos, and
compilation is handled by the Makefile.

To build the demos yourself:

#+BEGIN_SRC sh
git clone https://codeberg.org/prescheme/prescheme-demo.git
cd prescheme-demo
guix shell -m manifest.scm
make
#+END_SRC

** License

This code is licensed under BSD-3-Clause, the same as Scheme 48 and the
Pre-Scheme compiler.  See COPYING for details.
