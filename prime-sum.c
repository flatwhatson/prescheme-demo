#include "include/ps-init.h"
#include "prescheme.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

long sum_sieve(char *, long);
char *make_sieve(long);
long main(long, char **);

long sum_sieve(char *sieve_0X, long limit_1X) {
  long arg0K1;
  long arg0K0;
  long v_4X;
  long result_3X;
  long i_2X;
  {
    arg0K0 = 0;
    arg0K1 = 0;
    goto L114;
  }
L114 : {
  i_2X = arg0K0;
  result_3X = arg0K1;
  if ((i_2X == (1 + limit_1X))) {
    return result_3X;
  } else {
    if ((*(sieve_0X + i_2X))) {
      arg0K0 = (result_3X + i_2X);
      goto L117;
    } else {
      arg0K0 = result_3X;
      goto L117;
    }
  }
}
L117 : {
  v_4X = arg0K0;
  arg0K0 = (1 + i_2X);
  arg0K1 = v_4X;
  goto L114;
}
}
char *make_sieve(long limit_5X) {
  long arg0K1;
  long arg0K0;
  long j_11X;
  long i_10X;
  long j_9X;
  long i_8X;
  long i_7X;
  char *sieve_6X;
  {
    sieve_6X = (char *)malloc(sizeof(char) * (1 + limit_5X));
    arg0K0 = 0;
    goto L183;
  }
L183 : {
  i_7X = arg0K0;
  if ((i_7X == (1 + limit_5X))) {
    *(sieve_6X + 0) = 0;
    *(sieve_6X + 1) = 0;
    arg0K0 = 2;
    goto L151;
  } else {
    *(sieve_6X + i_7X) = 1;
    arg0K0 = (1 + i_7X);
    goto L183;
  }
}
L151 : {
  i_8X = arg0K0;
  j_9X = i_8X * i_8X;
  if ((limit_5X < j_9X)) {
    return sieve_6X;
  } else {
    if ((*(sieve_6X + i_8X))) {
      arg0K0 = i_8X;
      arg0K1 = j_9X;
      goto L136;
    } else {
      goto L166;
    }
  }
}
L136 : {
  i_10X = arg0K0;
  j_11X = arg0K1;
  if ((limit_5X < j_11X)) {
    goto L166;
  } else {
    *(sieve_6X + j_11X) = 0;
    arg0K0 = i_10X;
    arg0K1 = (j_11X + i_10X);
    goto L136;
  }
}
L166 : {
  arg0K0 = (1 + i_8X);
  goto L151;
}
}
long main(long argc_12X, char **argv_13X) {
  long arg0K0;
  long result_19X;
  long result_18X;
  char *sieve_17X;
  long limit_16X;
  FILE *err_15X;
  FILE *out_14X;
  {
    out_14X = stdout;
    err_15X = stderr;
    if ((2 == argc_12X)) {
      limit_16X = atol((*(argv_13X + 1)));
      if ((limit_16X < 0)) {
        ps_write_string("Limit must be non-negative", err_15X);
        {
          long ignoreXX;
          PS_WRITE_CHAR(10, err_15X, ignoreXX)
        }
        return 1;
      } else {
        if ((limit_16X < 2)) {
          arg0K0 = 0;
          goto L307;
        } else {
          sieve_17X = make_sieve(limit_16X);
          result_18X = sum_sieve(sieve_17X, limit_16X);
          free(sieve_17X);
          arg0K0 = result_18X;
          goto L307;
        }
      }
    } else {
      ps_write_string("usage: ", err_15X);
      ps_write_string((*(argv_13X + 0)), err_15X);
      ps_write_string(" <limit>", err_15X);
      {
        long ignoreXX;
        PS_WRITE_CHAR(10, err_15X, ignoreXX)
      }
      return 1;
    }
  }
L307 : {
  result_19X = arg0K0;
  ps_write_string("Sum of primes up to ", out_14X);
  ps_write_integer(limit_16X, out_14X);
  ps_write_string(" is ", out_14X);
  ps_write_integer(result_19X, out_14X);
  {
    long ignoreXX;
    PS_WRITE_CHAR(10, out_14X, ignoreXX)
  }
  return 0;
}
}
