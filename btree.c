#include "include/ps-init.h"
#include "prescheme.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct btree_node {
  struct btree_node *left;
  struct btree_node *right;
  long value;
};
static void deallocate_btree(struct btree_node *);
static char btree_equalP(struct btree_node *, struct btree_node *);
long main(void);

static void deallocate_btree(struct btree_node *t_0X) {
  struct btree_node *v_2X;
  struct btree_node *v_1X;
  {
    v_1X = t_0X->left;
    if ((NULL == v_1X)) {
      goto L91;
    } else {
      deallocate_btree((t_0X->left));
      goto L91;
    }
  }
L91 : {
  v_2X = t_0X->right;
  if ((NULL == v_2X)) {
    goto L105;
  } else {
    deallocate_btree((t_0X->right));
    goto L105;
  }
}
L105 : {
  free(t_0X);
  return;
}
}
static char btree_equalP(struct btree_node *a_3X, struct btree_node *b_4X) {
  struct btree_node *arg0K1;
  struct btree_node *arg0K0;
  char v_8X;
  char temp_7X;
  struct btree_node *b_6X;
  struct btree_node *a_5X;
  {
    arg0K0 = a_3X;
    arg0K1 = b_4X;
    goto L358;
  }
L358 : {
  a_5X = arg0K0;
  b_6X = arg0K1;
  temp_7X = a_5X == b_6X;
  if (temp_7X) {
    return temp_7X;
  } else {
    if (((a_5X->value) == (b_6X->value))) {
      v_8X = btree_equalP((a_5X->left), (b_6X->left));
      if (v_8X) {
        arg0K0 = (a_5X->right);
        arg0K1 = (b_6X->right);
        goto L358;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  }
}
}
long main(void) {
  struct btree_node *arg0K0;
  char v_23X;
  struct btree_node *c2_22X;
  struct btree_node *btree_node_21X;
  struct btree_node *b2_20X;
  struct btree_node *btree_node_19X;
  struct btree_node *a2_18X;
  struct btree_node *btree_node_17X;
  struct btree_node *c1_16X;
  struct btree_node *btree_node_15X;
  struct btree_node *b1_14X;
  struct btree_node *btree_node_13X;
  struct btree_node *a1_12X;
  struct btree_node *btree_node_11X;
  struct btree_node *null_10X;
  FILE *out_9X;
  {
    out_9X = stdout;
    null_10X = NULL;
    btree_node_11X = (struct btree_node *)malloc(sizeof(struct btree_node));
    if ((NULL == btree_node_11X)) {
      arg0K0 = btree_node_11X;
      goto L219;
    } else {
      btree_node_11X->left = null_10X;
      btree_node_11X->right = null_10X;
      btree_node_11X->value = 6;
      arg0K0 = btree_node_11X;
      goto L219;
    }
  }
L219 : {
  a1_12X = arg0K0;
  btree_node_13X = (struct btree_node *)malloc(sizeof(struct btree_node));
  if ((NULL == btree_node_13X)) {
    arg0K0 = btree_node_13X;
    goto L223;
  } else {
    btree_node_13X->left = null_10X;
    btree_node_13X->right = null_10X;
    btree_node_13X->value = 5;
    arg0K0 = btree_node_13X;
    goto L223;
  }
}
L223 : {
  b1_14X = arg0K0;
  btree_node_15X = (struct btree_node *)malloc(sizeof(struct btree_node));
  if ((NULL == btree_node_15X)) {
    arg0K0 = btree_node_15X;
    goto L227;
  } else {
    btree_node_15X->left = a1_12X;
    btree_node_15X->right = b1_14X;
    btree_node_15X->value = 4;
    arg0K0 = btree_node_15X;
    goto L227;
  }
}
L227 : {
  c1_16X = arg0K0;
  btree_node_17X = (struct btree_node *)malloc(sizeof(struct btree_node));
  if ((NULL == btree_node_17X)) {
    arg0K0 = btree_node_17X;
    goto L231;
  } else {
    btree_node_17X->left = null_10X;
    btree_node_17X->right = null_10X;
    btree_node_17X->value = 6;
    arg0K0 = btree_node_17X;
    goto L231;
  }
}
L231 : {
  a2_18X = arg0K0;
  btree_node_19X = (struct btree_node *)malloc(sizeof(struct btree_node));
  if ((NULL == btree_node_19X)) {
    arg0K0 = btree_node_19X;
    goto L235;
  } else {
    btree_node_19X->left = null_10X;
    btree_node_19X->right = null_10X;
    btree_node_19X->value = 5;
    arg0K0 = btree_node_19X;
    goto L235;
  }
}
L235 : {
  b2_20X = arg0K0;
  btree_node_21X = (struct btree_node *)malloc(sizeof(struct btree_node));
  if ((NULL == btree_node_21X)) {
    arg0K0 = btree_node_21X;
    goto L239;
  } else {
    btree_node_21X->left = a2_18X;
    btree_node_21X->right = b2_20X;
    btree_node_21X->value = 4;
    arg0K0 = btree_node_21X;
    goto L239;
  }
}
L239 : {
  c2_22X = arg0K0;
  v_23X = btree_equalP(c1_16X, c2_22X);
  if (v_23X) {
    ps_write_string("trees are equal\n", out_9X);
    goto L249;
  } else {
    ps_write_string("trees are not equal\n", out_9X);
    goto L249;
  }
}
L249 : {
  deallocate_btree(c1_16X);
  deallocate_btree(c2_22X);
  return 0;
}
}
