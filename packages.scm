(define-structure ps-utils
  (export (when :syntax)
          (unless :syntax)
          (receive :syntax)
          (define-record-type :syntax)
          (define-wrapper-type :syntax)
          zero?
          one?)
  (open prescheme
        ps-receive
        ps-record-types)
  (files (lib ps-utils)))

(define-structure ps-string
  (export string-copy!
          string-append
          string-repeat)
  (open prescheme)
  (files (lib ps-string)))

(define-structure ps-vector
  (export (vector-unfold :syntax) vector-unfold1 vector-unfold2 vector-unfold3
          (vector-fold :syntax) vector-fold1 vector-fold2 vector-fold3
          (vector-map! :syntax) vector-map1! vector-map2! vector-map3!
          (vector-map :syntax) vector-map1 vector-map2 vector-map3
          (vector-for-each :syntax) vector-for-each1 vector-for-each2 vector-for-each3
          vector-fill!)
  (open prescheme
        ps-receive)
  (files (lib ps-vector)))

(define-structure ps-grid
  (export create-grid
          destroy-grid
          grid-width
          grid-height
          grid-ref
          grid-fold
          grid-for-each
          grid-update!
          grid-unfold)
  (open prescheme
        ps-utils)
  (files (lib ps-grid)))

(define-structure ps-sdl2
  (export sdl-init
          sdl-quit
          sdl-get-error
          sdl-delay
          sdl-create-window
          sdl-destroy-window
          sdl-create-renderer
          sdl-destroy-renderer
          sdl-set-render-draw-color
          sdl-render-clear
          sdl-render-present
          sdl-render-fill-rect
          sdl-create-event
          sdl-destroy-event
          sdl-poll-event
          sdl-event-type
          SDL_INIT_VIDEO
          SDL_WINDOWPOS_CENTERED
          SDL_WINDOW_SHOWN
          SDL_RENDERER_ACCELERATED
          SDL_QUIT)
  (open prescheme
        ps-utils)
  (files (lib ps-sdl2)))

(define-structure hello (export main)
  (open prescheme)
  (files hello))

(define-structure append (export main string-append)
  (open prescheme
        ps-string)
  (files append))

(define-structure vecfun (export main)
  (open prescheme
        ps-string
        ps-vector)
  (files vecfun))

(define-structure recfun (export main write-vec2 write-rect)
  (open prescheme
        ps-record-types)
  (files recfun))

(define-structure btree (export main)
  (open prescheme
        ps-record-types)
  (files btree))

(define-structure game-of-life (export main)
  (open prescheme
        ps-utils
        ps-sdl2
        ps-grid)
  (files game-of-life))

(define-structure prime-sum (export main make-sieve sum-sieve)
  (open prescheme
        ps-utils
        ps-vector)
  (files prime-sum))
